import React from 'react';
import PropTypes from 'prop-types';

import { ReactComponent as Logo } from './icons/logo.svg';
import { ReactComponent as Favorite } from './icons/favorite.svg';
import { ReactComponent as Cart } from './icons/cart.svg';

import './Header.scss';

function Header({ qtyFavorites, qtyCart }) {
    return (
        <div className="header__wrapper">
            <div className="container">
                <div className="header">
                    <a href="/" className="header__logo">
                        <h1 className="header__logo--title">SHOES</h1>
                        <Logo className="header__logo--icon" />
                    </a>
                    <div className="header__actions">
                        <a className="actions__favorite--wrapp" href="/">
                            <Favorite className="actions__favorite" />
                            <span className="actions__favorite--qty">{qtyFavorites}</span>
                        </a>
                        <a className="actions__cart--wrapp" href="/">
                            <Cart className="actions__cart" />
                            <span className="actions__cart--qty">{qtyCart}</span>
                        </a>
                    </div>
                </div>
            </div>
        </div>
    );
}

Header.propTypes = {
    qtyFavorites: PropTypes.any,
    qtyCart: PropTypes.any,
};

export default Header;
